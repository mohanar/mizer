/*
 * DicomEdit: PrivateBlock
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.mizer.objects;

import org.nrg.dicom.mizer.tags.Tag;

/**
 * Encapsulate the group number and creator ID string that constitute a private block.
 *
 * .
 */
public class PrivateBlock {
    private final short group;
    private final String creatorID;

    public PrivateBlock( int tag, String creatorID) {
        this.group = (short) (tag >>> 16);
        this.creatorID = creatorID;
    }

    public PrivateBlock( short group, String creatorID) {
        this.group = group;
        this.creatorID = creatorID;
    }

    public short getGroup() {
        return group;
    }

    public String getCreatorID() {
        return creatorID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PrivateBlock that = (PrivateBlock) o;

        if (getGroup() != that.getGroup()) return false;
        return getCreatorID() != null ? getCreatorID().equals(that.getCreatorID()) : that.getCreatorID() == null;

    }

    @Override
    public int hashCode() {
        int result = getGroup();
        result = 31 * result + (getCreatorID() != null ? getCreatorID().hashCode() : 0);
        return result;
    }

    public static void main( String[] args) {
        PrivateBlock pb = new PrivateBlock( 0x00170010, "XYZ") ;

        System.out.println(Tag.padLeft(Integer.toHexString(pb.getGroup()), 4, '0'));
    }
}
