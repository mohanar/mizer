package org.nrg.dicom.mizer.objects;

import org.dcm4che2.data.*;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.io.DicomOutputStream;
import org.dcm4che2.util.TagUtils;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.tags.*;
import org.nrg.dicom.mizer.tags.Tag;
import org.nrg.dicom.mizer.values.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;
import java.util.zip.GZIPInputStream;

import static org.dcm4che2.util.TagUtils.isPrivateCreatorDataElement;
import static org.dcm4che2.util.TagUtils.isPrivateDataElement;

/**
 * Instantiate concrete implementations of DicomObjectI.
 */
public class DicomObjectFactory {
    public static DicomObjectI newInstance() {
        return new MizerDicomObject();
    }

    public static DicomObjectI newInstance(final File file) throws MizerException {
        return new MizerDicomObject(file);
    }

    public static DicomObjectI newInstance(final InputStream input) throws MizerException {
        return new MizerDicomObject(input);
    }

    /**
     * Create DicomObjectI from the provided dcm4che2 dicom object.
     *
     * This causes the dcm4ch4 lib to leak, but hey, backwards compatability.
     * This is for backwards compatibility with anonymize package.
     *
     * @param matchFile   TODO:  What is this? Ignored for now.
     * @param dicomObject The DICOM object to be processed.
     */
    public static DicomObjectI newInstance(final File matchFile, final DicomObject dicomObject) {
        return new MizerDicomObject(dicomObject);
    }

    public static DicomObjectI newInstance(final DicomObject dicomObject) {
        return new MizerDicomObject(dicomObject);
    }

    private static class MizerDicomObject implements DicomObjectI {

        private final DicomObject dobj;

        public MizerDicomObject() {
            this.dobj = new BasicDicomObject();
        }

        public MizerDicomObject(File f) throws MizerException {
            try (final InputStream fin = getInputStream(f); final DicomInputStream dis = new DicomInputStream(fin)) {
                dobj = dis.readDicomObject();
            } catch (IOException e) {
                throw new MizerException(e);
            }
        }

        public MizerDicomObject(final InputStream input) throws MizerException {
            try (final DicomInputStream dis = new DicomInputStream(input)) {
                dobj = dis.readDicomObject();
            } catch (IOException e) {
                throw new MizerException(e);
            }
        }

        public MizerDicomObject(final DicomObject dicomObject) {
            this.dobj = dicomObject;
        }

        /**
         * Return the value(s) at the specified tagPath as a String.
         *
         * The values of tags with multiplicity &gt; 1 are concatenated and separated with the DICOM value-separation character \.
         *
         * @param tagPath The {@link TagPath tag path} to retrieve.
         *
         * @return String representation of value(s) at specified tagPath. null if the tag does not exist, empty string if tag exists but is empty.
         *
         * @throws NumberFormatException if tagPath contains wildcard chars and does not resolve to a unique tag.
         */
        @Override
        public String getString(TagPath tagPath) {
            int[] tagArray = this.resolve(tagPath, false);
            return getString(tagArray);
        }

        /**
         * Return the value(s) at the specified tag as a String.
         *
         * The values of tags with multiplicity &gt; 1 are concatenated and separated with the DICOM value-separation character \.
         *
         * @param tag The tag to retrieve.
         *
         * @return String representation of value(s) at specified tag. null if the tag does not exist, empty string if tag exists but is empty.
         */
        @Override
        public String getString(int tag) {
            int[] tagArray = new int[1];
            tagArray[0] = tag;
            String v =  getString(tagArray);
            logger.debug("Got: " + TagPath.toString(tagArray) + " = " + v);
            return v;
        }

        /**
         * Return the value(s) at the specified tag array as a String.
         *
         * The values of tags with multiplicity &gt; 1 are concatenated and separated with the DICOM value-separation character \.
         *
         * @param tagArray An array of tags to evaluate.
         *
         * @return String representation of value(s) at specified tag. null if the tag does not exist, empty string if tag exists but is empty.
         */
        @Override
        public String getString(final int... tagArray) {
            String[] strings;

            // tags with VR == UN do not have an implementation of getStrings(). Are there other VRs that do this?
            // Is there a better way to detect tags with multiplicity > 1?
            // TODO Is there a cleaner way to handle tags with multiplicity > 1?
            logger.debug("Fetching: " + TagPath.toString(tagArray) );
            try {
                strings = dobj.getStrings(tagArray);
            } catch (UnsupportedOperationException e) {
                strings = new String[1];
                strings[0] = dobj.getString(tagArray);
            }
            if (strings == null) {
                return null;
            }
            StringBuilder sb = new StringBuilder();
            for (String s : strings) {
                sb.append(s).append("\\");
            }
            if (sb.length() > 0) {
                sb.deleteCharAt(sb.length() - 1);
            }
            logger.debug("Got: " + TagPath.toString(tagArray) + " = " + sb.toString());
            return sb.toString();
        }

        /**
         * Return the resolved private tag in the context of this dicom object, optionally create the tag if it does not exist.
         *
         * Will create the associated private creator ID if the tag being created requires it.
         *
         * @param tag        tag to be resolved, in range of 0xggggnnee where nn is ignored.
         * @param pvtCreator value of private creator ID of the owner of this tag.
         * @param create     create the resolved tag with empty value if the tag does not exist and this flag is true.
         *
         * @return the resolved private tag with value 0xggggbbee where bb is the block value reserved by the private creator.
         * or -1 if there is no reserved block.
         */
        @Override
        public int resolvePrivateTag(int tag, String pvtCreator, boolean create) {
            return dobj.resolveTag(tag, pvtCreator, create);
        }

        @Override
        public void assign(TagPath tagPath, String value) {
            if (tagPath.isSingular()) {
            	if (value == null) {
                    logger.debug("Failed to Assign value as its null: " + tagPath + " = " + value);
            		return;
            	}
                VRMap vrmap = VRMap.getVRMap();
                int tag = tagPath.getLastTag().asInt();
                VR vr = vrmap.vrOf(tag);

                if (VR.UN == vr) {
                    logger.warn("Warning: VR for tag " + TagUtils.toString(tag) + " is unknown. Using VR = UN.");
                    vr = VR.UN;
                }
//            dobj.putString( this.resolve(tagPath), vr, value);
                int[] ia = this.resolve(tagPath, true);
                logger.debug("Assign: " + TagPath.toString(ia) + " = " + value);
                dobj.putBytes(ia, vr, value.getBytes());

            } else {
                logger.warn("You can't assign to a tagpath that maps to multiple tags.");
            }
        }

        @Override
        public void assign(int tag, Value value) {
            VRMap vrmap = VRMap.getVRMap();
            VR vr = vrmap.vrOf(tag);
        	if (value == null) {
                logger.debug("Failed to Assign value as its null: " + tag + " = " + value);
        		return;
        	}

            if (vr == VR.UN) {
                logger.debug("Uh Oh. Assigning value to Dicom element with unknown VR.");
                logger.debug("Using VR = LO for now but fix this.");
                logger.debug("Add an entry in the data dictionary: " + TagUtils.toString(tag));
                vr = VR.LO;
            }
            dobj.putString(tag, vr, value.asString());
        }

        @Override
        public void assign(int[] tagPath, Value value) {
            VRMap vrmap = VRMap.getVRMap();
            int tag = tagPath[tagPath.length - 1];
            VR vr = vrmap.vrOf(tag);
        	if (value == null) {
                logger.debug("Failed to Assign value as its null: " + tag + " = " + value);
        		return;
        	}

            DicomObject tmpdobj = dobj;
            for (int i = 0; i < tagPath.length - 1; i++) {
                DicomElement element = tmpdobj.get(tagPath[i]);
                if (element == null) {
                    element = tmpdobj.putSequence(tagPath[i]);
                }
                org.dcm4che2.data.DicomObject newdobj = element.getDicomObject();
                if (newdobj == null) {
                    newdobj = new BasicDicomObject();
                    element.addDicomObject(newdobj);
                }
                tmpdobj = newdobj;
            }
            // TODO: Use the right VR.
            if (VR.UN == vr) {
                logger.debug("Warning: VR for tag " + TagUtils.toString(tag) + " is unknown.");
            }
            tmpdobj.putString(tagPath[tagPath.length - 1], vr, value.asString());
        }

        @Override
        public void delete(int tag) {
            dobj.remove(tag);
        }

        @Override
        public void delete(int[] tags) {
            dobj.remove(tags);
        }

        @Override
        public void delete(final TagPath tagPathToDelete) {
            if (tagPathToDelete.isSingular()) {
                logger.debug("Delete singular tagPath = " + tagPathToDelete);
                dobj.remove(this.resolve(tagPathToDelete, false));
            } else {
                DicomObjectVisitor deleteVisitor = new DicomObjectVisitor() {
                    @Override
                    public void visitTag(TagPath tagPath, DicomElementI dicomElement, DicomObjectI dicomObject) {
                        logger.debug("Testing " + tagPath + " against " + tagPathToDelete + " with regex " + tagPathToDelete.getRegex());
                        if (tagPathToDelete.isMatch(tagPath)) {
                            logger.debug("Delete tagPath = " + tagPath);
                            dobj.remove(resolve(tagPath, false));
//                        delete_( DicomObject.this.resolve(tagPath));
                        }
                    }

                    @Override
                    public void visitSequenceTag(TagPath tagPath, DicomElementI dicomElement, DicomObjectI dicomObject) {

                    }
                };

                deleteVisitor.visit(this);
                deleteEmptyItems(dobj);
            }
        }

        @Override
        public void deleteEmptyPrivateBlocks() {
            deleteEmptyPrivateBlocks(dobj);
        }

        @Override
        public DicomObject getDcm4che2Object() {
            return dobj;
        }

        @Override
        public void addMetaHeader() {
            dobj.putString(0x00020010, VR.UI, "1.2.840.10008.1.2.1");
        }

        @Override
        public Iterator<DicomElementI> iterator() {
            return new MizerDicomObject.DOIterator();
        }

        public int[] resolve(TagPath tagPath, boolean create) {
            List<Integer> tagArray = new ArrayList<>();
            DicomObject tmpdobj = dobj;

            for (org.nrg.dicom.mizer.tags.Tag t : tagPath.getTags()) {
                if (t instanceof TagSequence) {
                    TagSequence ts = (TagSequence) t;
                    org.nrg.dicom.mizer.tags.Tag tag = ts.getTag();
                    if (tag instanceof TagPrivate) {
                        TagPrivate tagPrivate = (TagPrivate) tag;
                        int resolvedTag = tmpdobj.resolveTag(tag.asInt(), tagPrivate.getPvtCreatorID(), create);
                        if (resolvedTag == -1) {
                            DicomElement de = tmpdobj.putSequence(tag.asInt());
                            tmpdobj.putString(tagPrivate.getPvtCreatorIDTag(), VR.LO, tagPrivate.getPvtCreatorID());
                            tmpdobj = de.getDicomObject();
                            resolvedTag = tagPrivate.getPvtCreatorIDTag();
                        }
                        tagArray.add(resolvedTag);
                        tagArray.add(ts.getItemNumberAsInt());
                    } else if (tag instanceof TagPublic) {
//                    TagPublic tagPublic = (TagPublic) tag;
                        DicomElement de = tmpdobj.get(tag.asInt());
                        if (de == null) {
                            de = tmpdobj.putSequence(tag.asInt());
                            de.addDicomObject(new BasicDicomObject());
                        }
                        tmpdobj = de.getDicomObject();
                        tagArray.add(tag.asInt());
                        tagArray.add(ts.getItemNumberAsInt());
                    }
                } else if (t instanceof TagPublic) {
                    tagArray.add(t.asInt());
                } else if (t instanceof TagPrivate) {
                    TagPrivate tagPrivate = (TagPrivate) t;
                    int resolvedTag = tmpdobj.resolveTag(t.asInt(), tagPrivate.getPvtCreatorID(), true);
                    if (resolvedTag == -1) {
                        tmpdobj.putString(tagPrivate.getPvtCreatorIDTag(), VR.LO, tagPrivate.getPvtCreatorID());
                        resolvedTag = tagPrivate.getPvtCreatorIDTag();
                    }
                    tagArray.add(resolvedTag);
                } else if (t instanceof TagPrivateCreator) {
                    TagPrivateCreator tpc = (TagPrivateCreator) t;
                    int resolvedTag = tmpdobj.resolveTag(t.asInt(), tpc.getPvtCreatorID(), false);
                    tagArray.add(resolvedTag);
                } else {
                    // TODO: Need good logging here.  Or die?
                    logger.debug("Error resolving tag of unimplemented type: " + t);
                }
            }

//        int[] array = tagArray.stream().mapToInt(i->i).toArray();
            return TagPath.copyListToArray(tagArray);
        }

        @Override
        public DicomObjectI getDicomObject(org.nrg.dicom.mizer.tags.Tag tag) {
            return null;
        }

        @Override
        public DicomElementI get(org.nrg.dicom.mizer.tags.Tag tag) {
            return null;
        }

        /**
         * Add the provided string value to the private tag.
         *
         * For a tag of form 0xggggbbee, putString ignores bb and "does the right thing", creating the private creator
         * ID element if needed and then puts the tag in the right block.
         *
         * Convenient for creating test DICOM objects.
         *
         * @param tag          The tag to write to.
         * @param pvtCreatorID The ID of the private tag creator.
         * @param value        The value to write to the tag.
         *
         * @return The ID of the resolved tag.
         */
        @Override
        public int putString(int tag, String pvtCreatorID, String value) {
            int t = dobj.resolveTag(tag, pvtCreatorID, true);
            // TODO: use right VR.
            dobj.putString(t, VR.LO, value);
            return t;
        }

        public void putString(int[] tags, String s) {
            dobj.putString(tags, VR.LO, s);
        }

        public void putString(int tag, String s) {
            dobj.putString(tag, VR.LO, s);
        }

        public void removeTag(int tag) {
            dobj.remove(tag);
        }

        public void removePrivateTag(int tag, String pvtCreatorID) {
            dobj.remove(dobj.resolveTag(tag, pvtCreatorID));
        }

        @Override
        public void write(OutputStream os) throws MizerException {
            try (final DicomOutputStream out = new DicomOutputStream(os)) {
                out.writeDicomFile(dobj);
                logger.debug(dobj.toString());
            } catch (IOException e) {
                throw new MizerException(e);
            }
        }

        @Override
        public boolean contains(Tag tag) {
            if (tag instanceof TagPublic) {
                return contains((TagPublic) tag);
            } else if (tag instanceof TagPrivate) {
                return contains((TagPrivate) tag);
            } else if (tag instanceof TagSequence) {
                return contains((TagSequence) tag);
            }
            return false;
        }

        @Override
        public boolean contains(TagPath tagPath) {
            for (final Tag t : tagPath.getTags()) {
                if (!contains(t)) {
                    return false;
                }
            }
            return true;
        }

        @Override
        public boolean contains(int tag) {
            return dobj.contains(tag);
        }

        /**
         * return true if an element exists at the tagpath.
         *
         * Handy for unit tests.
         *
         * @param tagArray The tag path to evaluate.
         *
         * @return Whether the element exists at the submitted tag path.
         */
        @Override
        public boolean contains(int[] tagArray) {
            return dobj.get(tagArray) != null;
        }

        @Override
        public String getPrivateCreator(int t) {
            return dobj.getPrivateCreator(t);
        }

        @Override
        public boolean isSequenceElement(int t) {
            return dobj.get(t).hasItems();
        }

        @Override
        public boolean isEmpty() {
            return dobj.isEmpty();
        }

        @Override
        public void deleteAllPrivateTags() {
            deleteAllPrivateTags(dobj);
        }

        @Override
        public String toString() {
            return dobj.toString();
        }

        private InputStream getInputStream(final File f) throws IOException {
            final InputStream fin = new FileInputStream(f);
            return f.getName().endsWith("gz") ? new GZIPInputStream(fin) : fin;
        }

        private void deleteEmptyItems(org.dcm4che2.data.DicomObject dicomObject) {
            Iterator<DicomElement> it = dicomObject.iterator();
            while (it.hasNext()) {
                DicomElement de = it.next();
                deleteEmptyItems(de);
            }
        }

        private void deleteEmptyItems(DicomElement de) {
            if (de.hasItems()) {
                for (int i = 0; i < de.countItems(); i++) {
                    org.dcm4che2.data.DicomObject dicomObject = de.getDicomObject(i);
                    if (dicomObject.isEmpty()) {
                        de.removeDicomObject(i);
                    } else {
                        deleteEmptyItems(dicomObject);
                    }
                }
            }
        }

        /**
         * Return a map of all private blocks to all their tags.
         *
         * @param dicomObject The DICOM object to evaluate.
         *
         * @return Return a map of private blocks to all their tags.
         */
        private Map<PrivateBlock, List<Integer>> listPrivateBlocks(org.dcm4che2.data.DicomObject dicomObject) {
            Map<PrivateBlock, List<Integer>> privateBlockTagMap = new HashMap<>();
            for (Iterator<DicomElement> it = dicomObject.iterator(); it.hasNext(); ) {
                int tag = it.next().tag();
                if (isPrivateDataElement(tag)) {
                    String pvtCreatorID = isPrivateCreatorDataElement(tag) ? dicomObject.getString(tag) : dicomObject.getPrivateCreator(tag);
                    PrivateBlock newPB = new PrivateBlock(tag, pvtCreatorID);
                    if (privateBlockTagMap.containsKey(newPB)) {
                        List<Integer> tagList = privateBlockTagMap.get(newPB);
                        tagList.add(tag);
                    } else {
                        List<Integer> tagList = new ArrayList<>();
                        tagList.add(tag);
                        privateBlockTagMap.put(newPB, tagList);
                    }
                }
            }
            return privateBlockTagMap;
        }

        private void deleteEmptyPrivateBlocks(org.dcm4che2.data.DicomObject dicomObject) {
            Map<PrivateBlock, List<Integer>> privateBlockTagMap = listPrivateBlocks(dicomObject);
            for (PrivateBlock pb : privateBlockTagMap.keySet()) {
                if (privateBlockTagMap.get(pb).size() == 1) {
                    int tag = privateBlockTagMap.get(pb).get(0);
                    dicomObject.remove(tag);
                }
            }
        }

        private void deleteAllPrivateTags(org.dcm4che2.data.DicomObject dicomObject) {

            for (Iterator<DicomElement> it = dicomObject.datasetIterator(); it.hasNext(); ) {
                DicomElement de = it.next();
                if (isPrivateDataElement(de.tag())) {
                    dobj.remove(de.tag());
                } else if (de.hasItems()) {
                    for (int i = 0; i < de.countItems(); i++) {
                        deleteAllPrivateTags(de.getDicomObject(i));
                    }
                }

            }
        }

        private boolean contains(TagPublic tagPublic) {
            return dobj.contains(tagPublic.asInt());
        }

        private boolean contains(TagPrivate tagPrivate) {
            int tagInt = dobj.resolveTag(tagPrivate.asInt(), tagPrivate.getPvtCreatorID(), false);
            return dobj.contains(tagInt);
        }

        private boolean contains(TagSequence tagSequence) {
            boolean b = false;
            DicomElement dicomElement = dobj.get(tagSequence.getTag().asInt());
            if (dicomElement != null) {
                b = (dicomElement.getDicomObject(tagSequence.getItemNumberAsInt()) != null);
            }
            return b;
        }

        private class DOIterator implements Iterator<DicomElementI> {

            Iterator<DicomElement> iterator = dobj.iterator();

            @Override
            public boolean hasNext() {
                return iterator.hasNext();
            }

            @Override
            public DicomElementI next() {
                return new MizerDicomElement(iterator.next());
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        }
    }

    private static class MizerDicomElement implements DicomElementI {

        private final DicomElement element;

        public MizerDicomElement(final DicomElement element) {
            this.element = element;
        }

        @Override
        public int tag() {
            return element.tag();
        }

        @Override
        public boolean hasItems() {
            return element.hasItems();
        }

        @Override
        public int countItems() {
            return element.countItems();
        }

        @Override
        public DicomObjectI getDicomObject(int i) {
            return new MizerDicomObject( element.getDicomObject( i));
        }

        @Override
        public void removeItem( int i) {
            element.removeDicomObject(i);
        }
    }

    private static final Logger logger = LoggerFactory.getLogger(DicomObjectFactory.class);
}
