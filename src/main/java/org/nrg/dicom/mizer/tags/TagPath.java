/*
 * DicomEdit: TagPath
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.mizer.tags;

import org.nrg.dicom.mizer.objects.DicomObjectI;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Contains the path for a DICOM tag.
 */
public class TagPath {
    private final List<Tag> tags;
    private String path;
    private String regex;

    private static final String VERBATIM_MODE_CHAR = "\"";
    private static final String ITEMNUMBER_WILDCARD_CHAR = "%";

    public TagPath( TagPath path) {
        this( path.getTags());
    }

    public TagPath() {
        this( new ArrayList<Tag>());
    }

    public TagPath( List<Tag> tags) {
//        this.tags = tags;
        this.tags = new ArrayList<>(tags);
        this.path = null;
        this.regex = null;
    }

    public TagPath(Tag tag) {
        this();
        tags.add(tag);
    }

    public TagPath addTag(Tag tag) {
        path = null;
        regex = null;
        tags.add( tag);
        return this;
    }

    public void addTags( List<Tag> tags) {
        path = null;
        regex = null;
        this.tags.addAll( tags);
    }

    public List<Tag> getTags() {
        return tags;
    }

    public Tag getLastTag() {
        return (tags.size() > 0)? tags.get(tags.size()-1): null;
    }

    /**
     * Return the tag path as an array of integers.
     *
     * [[sequence-tag, item-number] ...] tag
     * Private tags are in the default block.  It is expected that the  private tags will be resolved correctly
     * in the context of a specific dicom object.
     *
     * @return tag path as int array.
     */
    public int[] getTagsAsArray() {
        List<Integer> tagList = new ArrayList<>();

        for( Tag t: tags) {
            if( t instanceof TagSequence) {
                TagSequence ts = (TagSequence) t;
                tagList.add( ts.getTag().asInt());
                tagList.add( ts.getItemNumberAsInt());
            }
            else {
                tagList.add( t.asInt());
//                tagArray.add( 0);
            }
        }

//        int[] array = tagList.stream().mapToInt(i->i).toArray();
        int[] array = copyListToArray( tagList);
        return array;
    }

    /**
     * convert a list of Integers into an int array.
     *
     * This can be done more concisely and generically with 1.8 features.
     * {@code
     *     int[] array = integerList.stream().mapToInt(i->i).toArray();
     * }
     *
     * @param integerList The list of integer objects to convert.
     * @return  int array.
     */
    static public int[] copyListToArray( List<Integer> integerList) {
        int[] intArray = new int[integerList.size()];
        for( int i = 0; i < integerList.size(); i++) {
            intArray[i] = integerList.get(i);
        }
        return intArray;
    }

    public String getPath() {
        if( path == null) {
            StringBuilder sb = new StringBuilder();
            for (Tag t : tags) {
                sb.append( t.asString()).append(Tag.SEQUENCE_TAG_SEPARATOR);
            }
            if (sb.length() > 0) sb.deleteCharAt(sb.length() - 1);
            path = sb.toString();
        }
        return path;
    }

    public int size() {
        return tags.size();
    }

//    public int getTag(int i) {
//        return tags.get(i).asInteger();
//    }

    public String getRegex() {
        if (regex == null) regex = computeRegex();
        return regex;
    }

    public boolean isMatch(TagPath testpath) {
        if (regex == null) {
            regex = computeRegex();
        }
        return testpath.getPath().matches(regex);
    }

    public boolean isSingular() {
        boolean singular = true;
        for( Tag t: tags) {
            if ( ! t.isSingular()) {
                singular = false;
                break;
            }
        }
        return singular;
    }

    private String computeRegex() {
        StringBuilder sb = new StringBuilder();
        String[] tokens = getPath().split(VERBATIM_MODE_CHAR);
        for( int i = 0; i < tokens.length; i++) {
            if( i % 2 == 0) {
                sb.append( computeRegex( tokens[i]));
            }
            else {
                sb.append(Pattern.quote( VERBATIM_MODE_CHAR + tokens[i] + VERBATIM_MODE_CHAR));
            }
        }
        return sb.toString();
    }

    private String computeRegex(String s) {
        StringBuilder sb = new StringBuilder();
        char[] chars = s.toCharArray();
        for (char c: chars) {
            switch (c) {
                case '.':
                    sb.append("([0-9a-fA-F]{8}(\\[[0-9]+\\])?\\/){1}");
                    break;
                case '?':
                    sb.append("([0-9a-fA-F]{8}(\\[[0-9]+\\])?\\/)?");
                    break;
                case '*':
                    sb.append("([0-9a-fA-F]{8}(\\[[0-9]+\\])?\\/)*");
                    break;
                case '+':
                    sb.append("([0-9a-fA-F]{8}(\\[[0-9]+\\])?\\/)+");
                    break;
                case '/':
                    break;
                case 'X':
                case 'x':
                    sb.append("[0-9a-fA-F]");
                    break;
                case '#':
                    sb.append("[13579bdfBDF]");
                    break;
                case '@':
                    sb.append("[02468aceACE]");
                    break;
                case '%':
                    sb.append("[0-9]+");
                    break;
                case '[':
                    sb.append("\\[");
                    break;
                case ']':
                    sb.append("\\]\\/");
                    break;
                default:
                    sb.append(c);
            }
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        return getPath();
    }

    /**
     * Return a String representation of the specified integer tag array.
     *
     * The array is assumed to be tag1, itemnumber1, tag2, itemnumber2, tag3, ....
     * and format is tag1[itemnumber1]/tag2[itemnumber2]/tag3
     *
     * @param ia the tagpath as int array.
     * @return tagpath-formated string represention of int array.
     */
    public static String toString( int[] ia) {
        StringBuilder sb = new StringBuilder();

        for( int i = 0; i < ia.length; i++) {
            if( (i % 2) == 0) {
                sb.append( Integer.toHexString(ia[i]));
            }
            else {
                sb.append("[").append( Integer.toHexString(ia[i])).append("]");
            }
            sb.append("/");
        }
        sb.deleteCharAt( sb.length()-1);

        return sb.toString();
    }

    public Boolean isContainedIn(DicomObjectI dicomObjectI) {
        return null;
    }

}
