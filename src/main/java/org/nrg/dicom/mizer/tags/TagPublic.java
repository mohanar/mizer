/*
 * DicomEdit: TagPublic
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.mizer.tags;

/**
 * Implements a container for public tags.
 */
public class TagPublic extends Tag {

    private final String group;
    private final String element;
    private final boolean singular;

    public TagPublic(String group, String element) {
        this.group = group;
        this.element = element;
        singular = ! (hasWildCard( group) || hasWildCard( element));
    }

    public TagPublic( int tag) {
        String s = padLeft( Integer.toHexString( tag), 8, '0');
        this.group = s.substring(0,4);
        this.element = s.substring(4,8);
        this.singular = true;
    }

    public String getGroup() {
        return group;
    }

    public String getElement() {
        return element;
    }

    @Override
    public String asString() {
        return group + element;
    }

    public String toString() {
        return asString();
    }

    /**
     * Return the integer value of this tag.
     *
     * Throws NumberFormatException if this tag has wild cards and does not map to a unique integer.
     *
     * @return Returns the tag value as an integer.
     */
    public int asInt() { return Integer.parseInt( group + element, 16);}

    @Override
    public boolean isSingular() {
        return singular;
    }

}
