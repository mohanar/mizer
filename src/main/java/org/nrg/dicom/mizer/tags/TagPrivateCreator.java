/*
 * DicomEdit: TagPrivateCreator
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.mizer.tags;

/**
 * Implements a container for private tag creators.
 */
public class TagPrivateCreator extends Tag {

    private final String group;
    private final String pvtCreatorID;
    private final String element;
    private final boolean singular;

    private final static String DEFAULT_ELEMENT_BLOCK = "10";
    private final static String DEFAULT_PRIVATE_CREATOR_BLOCK = "00";
    private final static String DEFAULT_PRIVATE_CREATOR_ELEMENT = "0010";

    public TagPrivateCreator(String group, String pvtCreatorID, String element) {
        this.group = group;
        this.pvtCreatorID = pvtCreatorID;
        this.element = DEFAULT_PRIVATE_CREATOR_BLOCK + element;
        this.singular = ! (hasWildCard( group) || hasWildCard( element));
    }

    public TagPrivateCreator(int tag, String pvtCreatorID) {
        String s = padLeft( Integer.toHexString( tag), 8, '0');
        this.group = s.substring(0,4);
        this.element = DEFAULT_PRIVATE_CREATOR_BLOCK + s.substring(6,8);
        this.pvtCreatorID = pvtCreatorID;
        this.singular = true;
    }

    public String getGroup() {
        return group;
    }

    public String getPvtCreatorID() {
        return pvtCreatorID;
    }

    public int getPvtCreatorIDTag() {
        return Integer.parseInt( group + DEFAULT_PRIVATE_CREATOR_ELEMENT, 16);
    }

    public String getElement() {
        return element;
    }

    @Override
    public String asString() {
        return group + Tag.VERBATIM_MODE_CHAR + pvtCreatorID + Tag.VERBATIM_MODE_CHAR + element;
    }

    public String toString() {
        return asString();
    }

    /**
     * Return the integer value of this tag.
     *
     * Throws NumberFormatException if this tag has wild cards and does not map to a unique integer.
     *
     * This will always return the tag in block 10, i.e. gggg,10ee.  The actual tag value can only be resolved in the context
     * of the specific dicom object that contains this tag.
     * Use DicomObject's resolveTag method to do this.
     *
     * @return Returns the tag value as an integer.
     */
    public int asInt() { return Integer.parseInt( group + element, 16);}

    @Override
    public boolean isSingular() {
        return singular;
    }

}
