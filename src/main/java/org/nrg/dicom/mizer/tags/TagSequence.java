/*
 * DicomEdit: TagSequence
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.mizer.tags;

/**
 * Model Tags of type sequence (VR == SQ).
 *
 *
 */
public class TagSequence extends Tag {

    private final Tag tag;
    private final String itemNumber;
    private final boolean singular;

    public TagSequence( Tag tag, String itemNumber) {
        this.tag = tag;
//        singular = (itemNumber != null) && tag.isSingular();
        singular = ( ! hasWildCard(itemNumber)) && tag.isSingular();
        this.itemNumber = (itemNumber != null)? itemNumber: String.valueOf(Tag.ITEMNUMBER_WILDCARD_CHAR);
    }

    public TagSequence( Tag tag, int itemNumber) {
        this.tag = tag;
        this.singular = true;
        this.itemNumber = Integer.toString(itemNumber);
    }

    public Tag getTag() {
        return tag;
    }

    public String getItemNumber() {
        return itemNumber;
    }

    /**
     * Throws NumberFormat exception if the item number is a wild card and does not map to a unique integer.
     *
     * @return Returns the item number as an integer.
     */
    public int getItemNumberAsInt() {
        return Integer.parseInt( itemNumber);
    }

    @Override
    public String getGroup() {
        return tag.getGroup();
    }

    @Override
    public String getElement() {
        return tag.getElement();
    }

    @Override
    public String asString() {
        return tag.asString() + Tag.ITEMNUMBER_DELIMITER_START_CHAR + itemNumber + Tag.ITEMNUMBER_DELIMITER_END_CHAR;
    }

    public String toString() {
        return asString();
    }

    /**
     * Return the integer value of this tag.
     *
     * Throws NumberFormatException if this tag has wild cards and does not map to a unique integer.
     *
     * @return Returns the tag value as an integer.
     */
    public int asInt() { return Integer.parseInt( tag.getGroup() + tag.getElement(), 16);}

    @Override
    public boolean isSingular() {
        return singular;
    }
}
