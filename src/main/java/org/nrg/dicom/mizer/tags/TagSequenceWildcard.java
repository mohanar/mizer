/*
 * DicomEdit: TagSequenceWildcard
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.mizer.tags;

/**
 * Created by drm on 8/5/16.
 */
public class TagSequenceWildcard extends Tag {

    private String wildcardString;

    public TagSequenceWildcard( String wildcardString) {
        this.wildcardString = wildcardString;
    }

    @Override
    public String getGroup() {
        return null;
    }

    @Override
    public String getElement() {
        return null;
    }

    @Override
    public String asString() {
        return wildcardString;
    }

    @Override
    public int asInt() {
        return -1;
    }

    @Override
    public boolean isSingular() {
        return false;
    }
}
