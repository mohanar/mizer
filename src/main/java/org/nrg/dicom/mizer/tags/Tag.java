/*
 * DicomEdit: Tag
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.mizer.tags;

/**
 * The abstract base class for DICOM tags.
 */
public abstract class Tag {

    public static final char VERBATIM_MODE_CHAR              = '\"';
    public static final char ITEMNUMBER_WILDCARD_CHAR        = '%';
    public static final char ITEMNUMBER_DELIMITER_START_CHAR = '[';
    public static final char ITEMNUMBER_DELIMITER_END_CHAR   = ']';
    public static final char SEQUENCE_TAG_SEPARATOR          = '/';
    public static final char HEX_DIGIT_WILDCARD_CHAR1        = 'X';
    public static final char HEX_DIGIT_WILDCARD_CHAR2        = 'x';

    public abstract String getGroup();

    public abstract String getElement();

    public abstract String asString();

    /**
     * Return the integer value of this tag.
     *
     * Careful! Private tags only resolve to a unique integer in the context of a particular dicom object. This
     * returns private tags in the first block, "10ee" and may not be the appropriate mapping in a particular context.
     *
     * Throws NumberFormatException if this tag has wild cards and does not map to a unique integer.
     * Use "isSingular()" first to test this.
     *
     * @return The value of the tag as an integer.
     */
    public abstract int asInt();

    /**
     * Return true if this tag does not contain wildcards and thus maps to a unique integer.
     *
     * @return Whether the tag is singular.
     */
    public abstract boolean isSingular();

    /**
     * Return true if wild card char is detected in string.
     *
     * TODO: let the parser do this work.
     *
     * @param s String containing the group or element portion of a tag.
     *
     * @return true if the component contains a wild card char.
     */
    public boolean hasWildCard(String s) {
        boolean b = false;
        char[] chars = s.toCharArray();
        for (char c : chars) {
            switch (c) {
                case ITEMNUMBER_WILDCARD_CHAR:
                case HEX_DIGIT_WILDCARD_CHAR1:
                case HEX_DIGIT_WILDCARD_CHAR2:
                    b = true;
                    break;
                default:
                    b = false;
            }
        }
        return b;
    }

    /**
     * Adds enough of the the specified padding character to the submitted string to make the resulting string at least
     * as long as the specified minimum width.
     *
     * @param string   The string to be left-padded.
     * @param minWidth The width to which the string should be padded.
     * @param padChar  The character to use to left-pad the resulting string.
     *
     * @return The submitted string along with the required left padding.
     */
    public static String padLeft(String string, int minWidth, char padChar) {
        if (string.length() >= minWidth) {
            return string;
        } else {
            int padSize = minWidth - string.length();
            StringBuilder sb = new StringBuilder(minWidth);
            for (int i = 0; i < padSize; i++) {
                sb.append(padChar);
            }
            sb.append(string);
            return sb.toString();
        }
    }

}
