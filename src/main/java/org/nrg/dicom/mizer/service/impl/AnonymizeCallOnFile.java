package org.nrg.dicom.mizer.service.impl;

import com.google.common.io.ByteStreams;
import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.UID;
import org.dcm4che2.io.DicomCodingException;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.io.DicomOutputStream;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.objects.DicomObjectFactory;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.mizer.service.Mizer;
import org.nrg.dicom.mizer.service.MizerContext;
import org.nrg.dicomtools.utilities.DicomUtils;
import org.nrg.transaction.operations.CallOnFile;

import java.io.*;

/**
 * Apply the given script to the given dicom file on the filesystem.
 * From a PHI point-of-view all exceptions throws by this function
 * should be considered fatal.
 *
 * The dicom data is read from the given file, but the unchanged pixel data and
 * changed headers are written to a temporary file. The given file is replaced with the
 * temporary file if the anonymization process is successful.
 *
 * NOTE: The record and scriptId arguments indicate whether to record the application of this
 * script in the DICOM header and what the ID of the script is. For that reason if "record" is
 * false, the scriptId isn't checked and allowed to be null. If "record" is true, the "scriptId"
 * cannot be null and results in a runtime exception.
 *
 * This is really janky, but Java doesn't have pattern-matching on tuples and wrapping "record" and
 * "scriptId" into an object makes this function more opaque and harder to use.
 **/
public class AnonymizeCallOnFile extends CallOnFile<Void> {
    AnonymizeCallOnFile(final File dicomFile, final Mizer mizer, final MizerContext mizerContext) throws MizerException {
        _dicomFile = dicomFile;
        _mizer = mizer;
        _mizerContext = mizerContext;
    }

    // The dicom data is read from the given file, but the unchanged pixel data and
    // changed headers are written to a temporary file in the system's temp directory under
    // in the "anon_backup" directory. The given file is replaced with the
    // temporary file if the anonymization process is successful. The "anon_backup" directory
    // is left in place.
    @Override
    public Void call() throws Exception {

        try (final FileInputStream file = new FileInputStream(_dicomFile);
             final BufferedInputStream buffer = new BufferedInputStream(file);
             final DicomInputStream dicom = new DicomInputStream(buffer);
             final FileOutputStream output = new FileOutputStream(this.getFile().getAbsolutePath());
             final DicomOutputStream dicomOutput = new DicomOutputStream(output)) {

            dicomOutput.setAutoFinish(false);

            dicom.setHandler(DicomUtils.getMaxStopTagInputHandler());

            final DicomObjectI anonymized = _mizer.anonymize(DicomObjectFactory.newInstance(dicom.readDicomObject()), _mizerContext);
            final DicomObject  internal   = anonymized.getDcm4che2Object();

            final String tsuid = internal.getString(Tag.TransferSyntaxUID, UID.ImplicitVRLittleEndian);
            if (internal.contains(Tag.FileMetaInformationVersion)) {
                dicomOutput.writeFileMetaInformation(internal);
            } else {
                final DicomObject fileMetaInformation = new BasicDicomObject();
                fileMetaInformation.initFileMetaInformation(internal.getString(Tag.SOPClassUID), internal.getString(Tag.SOPInstanceUID), tsuid);
                dicomOutput.writeFileMetaInformation(fileMetaInformation);
            }
            dicomOutput.writeDataset(internal, tsuid);
            buffer.reset();
            ByteStreams.copy(buffer, output);
        } catch (DicomCodingException e) {
            throw new IOException(e);
        }
        return null;
    }

    private final File         _dicomFile;
    private final Mizer        _mizer;
    private final MizerContext _mizerContext;
}
