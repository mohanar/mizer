package org.nrg.dicom.mizer.service.impl;

import com.google.common.collect.Sets;
import org.dcm4che2.data.DicomObject;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.objects.DicomObjectFactory;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.mizer.service.Mizer;
import org.nrg.dicom.mizer.service.MizerContext;
import org.nrg.dicom.mizer.service.MizerService;
import org.nrg.dicom.mizer.variables.Variable;
import org.nrg.transaction.RollbackException;
import org.nrg.transaction.Run;
import org.nrg.transaction.TransactionException;
import org.nrg.transaction.operations.WorkOnCopyOp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.nrg.dicom.mizer.values.Value.EMPTY_VARIABLES;

/**
 * Basic Anonymization Service Provider
 *
 * Uses the first capable handler to process the request. Handlers are presented in descending max version order.
 * For example,
 * Mizer1 supports versions 4.0, Mizer2 supports versions '7.0, 6.0', Mizer3 supports versions 6.0. Search order will be
 * Mizer2, Mizer3, then Mizer1. Order is indeterminant if two handlers have the same max supported version.
 */
@Service
public class BaseMizerService implements MizerService {
    @Autowired
    public BaseMizerService(@SuppressWarnings("SpringJavaAutowiringInspection") final List<Mizer> mizers) {
        _mizers = mizers;
        Collections.sort(_mizers);
    }

    @Override
    public List<Mizer> getMizers() {
        return _mizers;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Integer> getScriptTags(final List<MizerContext> contexts) throws MizerException {
        return new HashSet<Integer>() {{
            for (final MizerContext context : contexts) {
                addAll(getScriptTags(context));
            }
        }};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Integer> getScriptTags(MizerContext context) throws MizerException {
        return findMizer(context).getScriptTags(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Variable> getReferencedVariables(final List<MizerContext> contexts) throws MizerException {
        if (contexts == null || contexts.isEmpty()) {
            return EMPTY_VARIABLES;
        }

        final LinkedHashSet<Variable> variables = Sets.newLinkedHashSet();
        for (final MizerContext context : contexts) {
            final Mizer mizer = findMizer(context);
            mizer.aggregate(context, variables);
        }
        return variables;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Variable> getReferencedVariables(final MizerContext context) throws MizerException {
        return getReferencedVariables(Collections.singletonList(context));
    }

    @Override
    public void anonymize(final DicomObjectI dobj, final MizerContext context) throws MizerException {
        final Mizer mizer = findMizer(context);

        if (mizer != null) {
            mizer.anonymize(dobj, context);
        } else {
            throw new MizerException(MessageFormat.format("Failed to find Mizer to anonymize dicom object in context {0}", context));
        }
    }

    @Override
    public void anonymize(final DicomObjectI dobj, final List<MizerContext> contexts) throws MizerException {
        for (final MizerContext context : contexts) {
            anonymize(dobj, context);
        }
    }

    @Override
    public void anonymize(DicomObjectI dobj, Properties anonContext) throws MizerException {
        MizerContext context = new MizerContextBase();
        context.add(anonContext);
        anonymize(dobj, context);
    }

    @Override
    public void anonymize(DicomObjectI dobj, Properties anonContext, String script) throws MizerException {
        MizerContextWithScript mctx = new MizerContextWithScript();
        mctx.setScript(script);
        mctx.add(anonContext);
        anonymize(dobj, mctx);
    }

    /**
     * Called from FileSystemSessionDataModifier
     *
     * @param dobj         The DICOM object.
     * @param project      The project.
     * @param subject      The subject.
     * @param session      The session.
     * @param scriptString The script as a string.
     *
     * @throws MizerException When an error occurs processing the script.
     */
    @Override
    public void anonymize(final DicomObject dobj, String project, String subject, String session, String scriptString) throws MizerException {
        final DicomObjectI doi = DicomObjectFactory.newInstance(dobj);
        final Properties anonContext = new Properties();
        anonContext.put("project", project);
        anonContext.put("subject", subject);
        anonContext.put("session", session);
        anonymize(doi, anonContext, scriptString);
    }

    @Override
    public void anonymize(File dicomFile, String project, String subject, String session, boolean record, long scriptId, List<String> script) throws MizerException {
        anonymize(dicomFile, getMizerContextWithScript(scriptId, project, subject, session, record, script));
    }

    @Override
    public void anonymize(File dicomFile, String project, String subject, String session, boolean record, long scriptId, InputStream scriptStream) throws MizerException {
        anonymize(dicomFile, getMizerContextWithScript(scriptId, project, subject, session, record, scriptStream));
    }

    /**
     * Called from GradualDicomImporter, AnonymizerA,
     *
     * @param dicomFile    The file containing the DICOM to be processed.
     * @param project      The project.
     * @param subject      The subject.
     * @param session      The session.
     * @param record       Whether the deidentification processing should be recorded in the DICOM output.
     * @param scriptId     The ID of the script (only used when <b>record</b> is true)
     * @param scriptString The script as a string.
     *
     * @throws MizerException When an error occurs processing the script.
     */
    @Override
    public void anonymize(File dicomFile, String project, String subject, String session, boolean record, long scriptId, String scriptString) throws MizerException {
        anonymize(dicomFile, getMizerContextWithScript(scriptId, project, subject, session, record, scriptString));
    }

    @Override
    public void anonymize(File dicomFile, String project, String subject, String session, boolean record, InputStream script) throws MizerException {
        anonymize(dicomFile, project, subject, session, record, 0L, script);
    }

    @Override
    public void anonymize(File dicomFile, String project, String subject, String session, boolean record, List<String> script) throws MizerException {
        anonymize(dicomFile, project, subject, session, record, 0L, script);
    }

    @Override
    public void anonymize(File dicomFile, List<MizerContext> contexts) throws MizerException {
        for (MizerContext context : contexts) {
            anonymize(dicomFile, context);
        }
    }

    /**
     * Find the first mizer that understands the anonymization context or throw error if none found.
     *
     * @param context {@link MizerContext}
     *
     * @return the first mizer that understands the context or null if none found.
     *
     * @throws MizerException if there are no matching Mizers.
     */
    protected Mizer findMizer(MizerContext context) throws MizerException {
        for (Mizer m : _mizers) {
            if (m.understands(context)) {
                return m;
            }
        }
        if (context instanceof MizerContextWithScript) {
            final Matcher matcher = INVALID_VERSION_FORMAT.matcher(((MizerContextWithScript) context).getScriptAsString());
            if (matcher.find()) {
                throw new MizerException(String.format(INVALID_VERSION_MESSAGE, matcher.group("expression")));
            }
        }
        throw new MizerException("The Mizer service failed to find a Mizer implementation that knows how to handle your script");
    }

    /**
     * Anonymize the DICOM File, rollback if error occurs.
     *
     * This is the lowest level function of this type.  Others are convenience methods that call this one.
     *
     * @param dicomFile The given dicom file on the filesystem
     * @param context   {@link MizerContext} additional context.
     *
     * @throws MizerException When an error occurs evaluating the script.
     */
    // The dicom data is read from the given file, but the unchanged pixel data and
    // changed headers are written to a temporary file in the system's temp directory under
    // in the "anon_backup" directory. The given file is replaced with the
    // temporary file if the anonymization process is successful. The "anon_backup" directory
    // is left in place.
    private void anonymize(final File dicomFile, final MizerContext context) throws MizerException {
        try {
            final Mizer mizer = findMizer(context);
            final AnonymizeCallOnFile acof = new AnonymizeCallOnFile(dicomFile, mizer, context);
            final File tmpdir = new File(System.getProperty("java.io.tmpdir"), "anon_backup");
            final WorkOnCopyOp anonymizeOp = new WorkOnCopyOp(dicomFile, tmpdir, acof);
            Run.runTransaction(anonymizeOp);
        } catch (RollbackException | TransactionException e) {
            throw new MizerException(e);
        }
    }

    @Nonnull
    private MizerContextWithScript getMizerContextWithScript(final long scriptId, final String project, final String subject, final String session, final boolean record, final Object script) throws MizerException {
        final MizerContextWithScript mizerContext = new MizerContextWithScript();
        mizerContext.setScriptId(scriptId);
        mizerContext.setElement("project", project);
        mizerContext.setElement("subject", subject);
        mizerContext.setElement("session", session);
        if (script instanceof List) {
            //noinspection unchecked
            mizerContext.setScript((List<String>) script);
        } else if (script instanceof InputStream) {
            mizerContext.setScript((InputStream) script);
        } else {
            mizerContext.setScript(script.toString());
        }
        mizerContext.setRecord(record);
        return mizerContext;
    }

    private static final Pattern   INVALID_VERSION_FORMAT  = Pattern.compile("^.*(?<expression>version\\s*[:]?=\\s*\"[\\d.]+\").*$", Pattern.MULTILINE);
    private static final String    INVALID_VERSION_MESSAGE = "The Mizer service failed to find a Mizer implementation that knows how to handle your"
                                                             + "script, but also found what appears to be a malformed version declaration. The "
                                                             + "version declaration should be use the format 'version \"X.Y\"', where X.Y is a valid "
                                                             + "version such as \"6.1\". The statement in your script is: %s";

    private final List<Mizer> _mizers;
}
