package org.nrg.dicom.mizer.service;

import org.dcm4che2.data.DicomObject;
import org.nrg.dicom.mizer.exceptions.MizerContextException;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.mizer.variables.Variable;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.Set;

/**
 * The Mizer Service interface (Mizer as in anonyMIZER).
 *
 * At your service for all things anonymized.
 *
 * Implementations of MizerService manage multiple instances of {@link Mizer}, which are the service handlers. The
 * MizerService chooses a particular handler based on the {@link MizerContext} supplied with the request.
 *
 */
public interface MizerService {

    /**
     * Anonymize the DICOM object in-place in the given context.
     *
     * @param dobj {@link DicomObjectI} the DICOM object being anonyimized.
     * @param context {@link MizerContext} supplies additional context to the {@link Mizer}
     * @throws MizerException when shit hits the fan.
     */
    void anonymize(DicomObjectI dobj, MizerContext context) throws MizerException;
    void anonymize(DicomObjectI dobj, List<MizerContext> contexts) throws MizerException;

    void anonymize(DicomObjectI dobj, Properties anonContext) throws MizerException;
    void anonymize(DicomObjectI dobj, Properties anonContext, String script) throws MizerException;

    /**
     * Called from FileSystemSessionDataModifier
     * @param dicomFile
     * @param project
     * @param subject
     * @param session
     * @param script
     * @throws MizerException
     */
    // leak dcm4che2
    void anonymize(DicomObject dicomFile,
                   String project,
                   String subject,
                   String session,
                   String script) throws MizerException;

    /**
     * Anonymize the dicom file by applying the given edit script.
     *
     * All exceptions throws by this function should be considered fatal.
     *
     * The caller should then make sure to delete the dicom file
     * from the filesystem.
     *
     * NOTE: The record and scriptId arguments indicate whether to record the application of this
     * script in the DICOM header and what the ID of the script is. For that reason if "record" is
     * false, the scriptId isn't checked and allowed to be null. If "record" is true, the "scriptId"
     * cannot be null and results in a runtime exception.
     *
     * This method which accepts the script as a file (and not a string from the database) so most of the intended usage will
     * not require recording to the DICOM header but the functionality is here just in case.
     *
     * This is really janky, but Java doesn't have pattern-matching on tuples and wrapping "record" and
     * "scriptId" into an object makes this function more opaque and harder to use.
     *
     * @param dicomFile The DICOM file
     * @param project   The project to which this file belongs
     * @param subject   The subject to which this file belongs
     * @param session   The session to which this file belongs
     * @param record    A boolean indicating whether to record the application of these scripts in the DICOM header
     * @param scriptId  The database ID of the submitted script. Only required if the "record" flag is true.
     * @param scriptStream    The script to be applied to the given DICOM file
     * @throws IllegalArgumentException When an invalid argument is passed to the anonymizer.
     */
    void anonymize(File dicomFile,
                   String project,
                   String subject,
                   String session,
                   boolean record,
                   long scriptId,
                   InputStream scriptStream) throws MizerException;

    void anonymize(File dicomFile,
                   String project,
                   String subject,
                   String session,
                   boolean record,
                   long scriptId,
                   String scriptString) throws MizerException;

    void anonymize(File dicomFile,
                   String project,
                   String subject,
                   String session,
                   boolean record,
                   long scriptId,
                   List<String> script) throws MizerException;

    void anonymize(File dicomFile,
                   String project,
                   String subject,
                   String session,
                   boolean record,
                   InputStream scriptStream) throws MizerException;

    void anonymize(File dicomFile,
                   String project,
                   String subject,
                   String session,
                   boolean record,
                   List<String> script) throws MizerException;

    void anonymize(File dicomFile,
                   List<MizerContext> scripts) throws MizerException;

    List<Mizer> getMizers();

    /**
     * Returns a set containing all of the DICOM tags referenced in the list of scripts. The mizer service determines
     * the appropriate {@link Mizer mizer implementation} to call for each script and delegates the evaluation and
     * extraction to that mizer's {@link Mizer#getScriptTags(List)} method.
     *
     * @param contexts The scripts to evaluate.
     *
     * @return All of the DICOM tags referenced in the submitted scripts.
     *
     * @throws MizerException When an error occurs evaluating the submitted script.
     */
    Set<Integer> getScriptTags(final List<MizerContext> contexts) throws MizerException;

    /**
     * Returns a set containing all of the DICOM tags referenced in the submitted script.  The mizer service determines
     * the appropriate {@link Mizer mizer implementation} to call for the script and delegates the evaluation and
     * extraction to that mizer's {@link Mizer#getScriptTags(MizerContext)} method.
     *
     * @param context The script to evaluate.
     *
     * @return All of the DICOM tags referenced in the submitted script.
     *
     * @throws MizerException When an error occurs evaluating the submitted script.
     */
    Set<Integer> getScriptTags(final MizerContext context) throws MizerException;

    /**
     * Returns a set containing all of the variables referenced in the submitted scripts.
     *
     * @param contexts The scripts to evaluate.
     *
     * @return All of the variables referenced in the submitted scripts.
     *
     * @throws MizerContextException When an error occurs evaluating the submitted scripts.
     */
    Set<Variable> getReferencedVariables(final List<MizerContext> contexts) throws MizerException;

    /**
     * Returns a set containing all of the variables referenced in the submitted script. The mizer service determines
     * the appropriate {@link Mizer mizer implementation} to call for the script and delegates the evaluation and
     * extraction to that mizer's {@link Mizer#getScriptTags(MizerContext)} method.
     *
     * @param context The script to evaluate.
     *
     * @return All of the variables referenced in the submitted script.
     *
     * @throws MizerContextException When an error occurs evaluating the submitted script.
     */
    Set<Variable> getReferencedVariables(final MizerContext context) throws MizerException;
}
