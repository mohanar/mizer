/*
 * mizer: org.nrg.dicom.mizer.service.impl.test.TestMizer
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.mizer.service.impl.test;

import org.dcm4che2.data.Tag;
import org.nrg.dicom.mizer.exceptions.MizerContextException;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.mizer.service.MizerContext;
import org.nrg.dicom.mizer.service.VersionString;
import org.nrg.dicom.mizer.service.impl.AbstractMizer;
import org.nrg.dicom.mizer.service.impl.MizerContextWithScript;
import org.nrg.dicom.mizer.variables.BasicVariable;
import org.nrg.dicom.mizer.variables.Variable;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by davidmaffitt on 4/7/17.
 */
//@Component
public class TestMizer extends AbstractMizer {

    private static final List<VersionString> supportedVersions = Collections.singletonList(new VersionString("6.0"));

    public TestMizer() {
        super(supportedVersions);
    }

    @Override
    public boolean understands( MizerContext context) throws MizerException {
        return true;
    }

    @Override
    public void aggregate(final MizerContext context, final Set<Variable> variables) throws MizerContextException {

    }

    @Override
    protected void anonymizeImpl(final DicomObjectI dicomObject, final MizerContextWithScript context) throws MizerException {
        return;
    }

    @Override
    protected String getMeaning() {
        return "Test Mizer";
    }

    @Override
    protected String getSchemeDesignator() {
        return "Test";
    }

    @Override
    protected String getSchemeVersion() {
        return "1.0";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Integer> getScriptTags(final List<MizerContext> contexts) {
        return new HashSet<Integer>() {{
            for (final MizerContext context : contexts) {
                addAll(getScriptTags(context));
            }
        }};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Integer> getScriptTags(final MizerContext context) {
        return Collections.singleton(Tag.PixelData - 1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void getReferencedVariables(final MizerContext context, final Set<Variable> variables) throws MizerContextException {
        variables.add(new BasicVariable("project"));
        variables.add(new BasicVariable("subject"));
        variables.add(new BasicVariable("session"));
    }
}
